# Build & Deploy

```bash
    sam validate; sam build; sam local start-api
    sam package --s3-bucket t-resume > template-prod.yaml
    sam deploy --template-file template-prod.yaml --stack-name t-resume --capabilities CAPABILITY_IAM
```
