import json
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()
logger.disabled = False


resume_data = {
    "Ping": "OK",
    "Status": "Yes",
    "Source": "https://gitlab.com/tathagata1/emx/tree/master",
    "Position": "2018-2515",
    "Years": "8",
    "Resume": "https://tathagata.me/resume",
    "Degree": "MS, B-Tech",
    "Phone": "redacted",
    "Name": "Tathagata Dasgupta",
    "Email Address": "mail at tathagata dot me",
    "Referrer": "Hired",
}


def load_puzzle_input(key_puzzle_string):
    logger.info(key_puzzle_string)
    puzzle = key_puzzle_string.split("\n")[2:6]
    entries = []
    for i, p in enumerate(puzzle):
        entries.append(list(p)[1:])
        entries[i][i] = "="

    return entries


def find_paired_rows(rows):
    lt_pairs, gt_pairs = [], []
    for i, row in enumerate(rows):
        if "<" in row:
            lt_pairs.append((i, row))
        if ">" in row:
            gt_pairs.append((i, row))
    return lt_pairs, gt_pairs


def reverse(char):
    return ">" if char == "<" else "<"


def _copy(char, list1, list2):
    for i in range(4):
        if list1[i] == "-" and list2[i] == "=":
            list1[i] = reverse(char)
        elif list1[i] == "-":
            list1[i] = char
    return list1, list2


def copy_chars(char, paired_lists):
    tuple1, tuple2 = paired_lists
    list1, list2 = tuple1[1], tuple2[1]

    list1, list2 = _copy(char, list1, list2)
    list2, list1 = _copy(char, list2, list1)

    return tuple1[0], "".join(list1), tuple2[0], "".join(list2)


def lookup_resume_data(question):
    return resume_data[question]


def solve_puzzle(description):
    result = [""] * 4
    rows = load_puzzle_input(description)
    lt_pairs, gt_pairs = find_paired_rows(rows)

    row1, v1, row2, v2 = copy_chars("<", lt_pairs)
    result[row1], result[row2] = v1, v2

    row1, v1, row2, v2 = copy_chars(">", gt_pairs)
    result[row1], result[row2] = v1, v2

    return (
        f" ABCD\n"
        f"A{result[0]}\n"
        f"B{result[1]}\n"
        f"C{result[2]}\n"
        f"D{result[3]}\n"
    )


def lambda_handler(event, context):
    query = event["queryStringParameters"]

    question, description = query["q"], query["d"]
    logger.info(f"{question}, {description}")
    if question == "Puzzle":
        response = solve_puzzle(description)
    else:
        response = lookup_resume_data(question)

    return {
        "statusCode": 200,
        "headers": {"content-type": "text/html"},
        "body": response,
    }
