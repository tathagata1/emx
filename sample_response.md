{'d': 'Please return OK so that I know your service works.', 'q': 'Ping'}
END RequestId: e2369c54-6bb4-1791-8104-af3301e3bffc
REPORT RequestId: e2369c54-6bb4-1791-8104-af3301e3bffc  Duration: 6.77 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:07 127.0.0.1 - - [22/Sep/2019 12:42:07] "GET /resume?q=Ping&d=Please+return+OK+so+that+I+know+your+service+works. HTTP/1.0" 200 -
2019-09-22 12:42:07 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:08 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 7d0a211c-4685-1d8d-556a-bbd2c916082e Version: $LATEST
{'d': 'Can you provide proof of eligibility to work in the US?', 'q': 'Status'}
END RequestId: 7d0a211c-4685-1d8d-556a-bbd2c916082e
REPORT RequestId: 7d0a211c-4685-1d8d-556a-bbd2c916082e  Duration: 4.33 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:09 127.0.0.1 - - [22/Sep/2019 12:42:09] "GET /resume?q=Status&d=Can+you+provide+proof+of+eligibility+to+work+in+the+US%3F HTTP/1.0" 200 -
2019-09-22 12:42:09 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:10 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 94711c45-6d50-1e95-7639-132a67cf7cad Version: $LATEST
{'d': 'Please provide a URL where we can download the source code of your '
      'resume submission web service.',
 'q': 'Source'}
END RequestId: 94711c45-6d50-1e95-7639-132a67cf7cad
REPORT RequestId: 94711c45-6d50-1e95-7639-132a67cf7cad  Duration: 14.17 ms      Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:12 127.0.0.1 - - [22/Sep/2019 12:42:12] "GET /resume?q=Source&d=Please+provide+a+URL+where+we+can+download+the+source+code+of+your+resume+submission+web+service. HTTP/1.0" 200 -
2019-09-22 12:42:12 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:13 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 44cf32d2-309c-16c6-92f3-1db7ef7a82a6 Version: $LATEST
{'d': 'Which position at Balihoo are you applying for?', 'q': 'Position'}
END RequestId: 44cf32d2-309c-16c6-92f3-1db7ef7a82a6
REPORT RequestId: 44cf32d2-309c-16c6-92f3-1db7ef7a82a6  Duration: 10.49 ms      Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:14 127.0.0.1 - - [22/Sep/2019 12:42:14] "GET /resume?q=Position&d=Which+position+at+Balihoo+are+you+applying+for%3F HTTP/1.0" 200 -
2019-09-22 12:42:14 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:15 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: f4ec688d-d72e-12eb-33b9-06154904d17c Version: $LATEST
{'d': 'How many years of software development experience do you have?',
 'q': 'Years'}
END RequestId: f4ec688d-d72e-12eb-33b9-06154904d17c
REPORT RequestId: f4ec688d-d72e-12eb-33b9-06154904d17c  Duration: 6.16 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:17 127.0.0.1 - - [22/Sep/2019 12:42:17] "GET /resume?q=Years&d=How+many+years+of+software+development+experience+do+you+have%3F HTTP/1.0" 200 -
2019-09-22 12:42:17 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:17 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: b6650ad9-352e-1994-08a7-1a9cb241eadc Version: $LATEST
{'d': 'Please provide a URL where we can download your resume and cover '
      'letter.',
 'q': 'Resume'}
END RequestId: b6650ad9-352e-1994-08a7-1a9cb241eadc
REPORT RequestId: b6650ad9-352e-1994-08a7-1a9cb241eadc  Duration: 12.22 ms      Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:19 127.0.0.1 - - [22/Sep/2019 12:42:19] "GET /resume?q=Resume&d=Please+provide+a+URL+where+we+can+download+your+resume+and+cover+letter. HTTP/1.0" 200 -
2019-09-22 12:42:19 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:20 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: b986ca28-7eaf-1774-a2d7-06d749a9c11e Version: $LATEST
{'d': 'Please list your relevant university degree(s).', 'q': 'Degree'}
END RequestId: b986ca28-7eaf-1774-a2d7-06d749a9c11e
REPORT RequestId: b986ca28-7eaf-1774-a2d7-06d749a9c11e  Duration: 5.87 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:21 127.0.0.1 - - [22/Sep/2019 12:42:21] "GET /resume?q=Degree&d=Please+list+your+relevant+university+degree%28s%29. HTTP/1.0" 200 -
2019-09-22 12:42:21 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:22 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 29ca523a-8499-1ea4-1ecb-ac7edc450c1e Version: $LATEST
{'d': 'Please provide a phone number we can use to reach you.', 'q': 'Phone'}
END RequestId: 29ca523a-8499-1ea4-1ecb-ac7edc450c1e
REPORT RequestId: 29ca523a-8499-1ea4-1ecb-ac7edc450c1e  Duration: 12.47 ms      Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:23 127.0.0.1 - - [22/Sep/2019 12:42:23] "GET /resume?q=Phone&d=Please+provide+a+phone+number+we+can+use+to+reach+you. HTTP/1.0" 200 -
2019-09-22 12:42:23 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:24 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 0abea130-606b-11c6-0931-0edc1b0d0fb8 Version: $LATEST
{'d': 'Please solve this puzzle:\n ABCD\nA=<--\nB-=<-\nC->--\nD-->-\n',
 'q': 'Puzzle'}
END RequestId: 0abea130-606b-11c6-0931-0edc1b0d0fb8
REPORT RequestId: 0abea130-606b-11c6-0931-0edc1b0d0fb8  Duration: 8.23 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:26 127.0.0.1 - - [22/Sep/2019 12:42:26] "GET /resume?q=Puzzle&d=Please+solve+this+puzzle%3A%0A+ABCD%0AA%3D%3C--%0AB-%3D%3C-%0AC-%3E--%0AD--%3E-%0A HTTP/1.0" 200 -
2019-09-22 12:42:26 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:26 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 6744117c-5fd4-16b7-915c-a8a8d89c34ec Version: $LATEST
{'d': 'Please solve this puzzle:\n ABCD\nA=->-\nB>=--\nC<---\nD--<-\n',
 'q': 'Puzzle'}
END RequestId: 6744117c-5fd4-16b7-915c-a8a8d89c34ec
REPORT RequestId: 6744117c-5fd4-16b7-915c-a8a8d89c34ec  Duration: 10.67 ms      Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:28 127.0.0.1 - - [22/Sep/2019 12:42:28] "GET /resume?q=Puzzle&d=Please+solve+this+puzzle%3A%0A+ABCD%0AA%3D-%3E-%0AB%3E%3D--%0AC%3C---%0AD--%3C-%0A HTTP/1.0" 200 -
2019-09-22 12:42:28 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:29 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 9aa31311-1c6d-16ee-c9a3-6930555ce04e Version: $LATEST
{'d': 'Please solve this puzzle:\n ABCD\nA-<--\nB-=-<\nC--=>\nD->--\n',
 'q': 'Puzzle'}
END RequestId: 9aa31311-1c6d-16ee-c9a3-6930555ce04e
REPORT RequestId: 9aa31311-1c6d-16ee-c9a3-6930555ce04e  Duration: 9.41 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:30 127.0.0.1 - - [22/Sep/2019 12:42:30] "GET /resume?q=Puzzle&d=Please+solve+this+puzzle%3A%0A+ABCD%0AA-%3C--%0AB-%3D-%3C%0AC--%3D%3E%0AD-%3E--%0A HTTP/1.0" 200 -
2019-09-22 12:42:31 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:31 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 9485cbe4-e903-111f-b9db-5a93da76ac22 Version: $LATEST
{'d': 'What is your email address?', 'q': 'Email Address'}
END RequestId: 9485cbe4-e903-111f-b9db-5a93da76ac22
REPORT RequestId: 9485cbe4-e903-111f-b9db-5a93da76ac22  Duration: 6.58 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:33 127.0.0.1 - - [22/Sep/2019 12:42:33] "GET /resume?q=Email+Address&d=What+is+your+email+address%3F HTTP/1.0" 200 -
2019-09-22 12:42:33 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:33 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 5e40aa59-a429-1972-c834-a856f3b33c0b Version: $LATEST
{'d': 'How did you hear about this position?', 'q': 'Referrer'}
END RequestId: 5e40aa59-a429-1972-c834-a856f3b33c0b
REPORT RequestId: 5e40aa59-a429-1972-c834-a856f3b33c0b  Duration: 8.39 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:35 127.0.0.1 - - [22/Sep/2019 12:42:35] "GET /resume?q=Referrer&d=How+did+you+hear+about+this+position%3F HTTP/1.0" 200 -
2019-09-22 12:42:35 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:36 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: c37b7cd9-48d3-10d0-b3cd-0d4eccb9177c Version: $LATEST
{'d': 'Please solve this puzzle:\n ABCD\nA=<--\nB>---\nC->=-\nD<---\n',
 'q': 'Puzzle'}
END RequestId: c37b7cd9-48d3-10d0-b3cd-0d4eccb9177c
REPORT RequestId: c37b7cd9-48d3-10d0-b3cd-0d4eccb9177c  Duration: 7.84 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:37 127.0.0.1 - - [22/Sep/2019 12:42:37] "GET /resume?q=Puzzle&d=Please+solve+this+puzzle%3A%0A+ABCD%0AA%3D%3C--%0AB%3E---%0AC-%3E%3D-%0AD%3C---%0A HTTP/1.0" 200 -
2019-09-22 12:42:37 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:38 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 52b9a7df-a8ef-1d48-f480-460390ded2b9 Version: $LATEST
{'d': 'Please solve this puzzle:\n ABCD\nA-->-\nB-=-<\nC--=>\nD--<-\n',
 'q': 'Puzzle'}
END RequestId: 52b9a7df-a8ef-1d48-f480-460390ded2b9
REPORT RequestId: 52b9a7df-a8ef-1d48-f480-460390ded2b9  Duration: 5.03 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:39 127.0.0.1 - - [22/Sep/2019 12:42:39] "GET /resume?q=Puzzle&d=Please+solve+this+puzzle%3A%0A+ABCD%0AA--%3E-%0AB-%3D-%3C%0AC--%3D%3E%0AD--%3C-%0A HTTP/1.0" 200 -
2019-09-22 12:42:40 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:40 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 7c87c91d-ac03-162a-413b-87954135af76 Version: $LATEST
{'d': 'What is your full name?', 'q': 'Name'}
END RequestId: 7c87c91d-ac03-162a-413b-87954135af76
REPORT RequestId: 7c87c91d-ac03-162a-413b-87954135af76  Duration: 7.66 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:42 127.0.0.1 - - [22/Sep/2019 12:42:42] "GET /resume?q=Name&d=What+is+your+full+name%3F HTTP/1.0" 200 -
2019-09-22 12:42:42 Invoking app.lambda_handler (python3.7)

Fetching lambci/lambda:python3.7 Docker container image......
2019-09-22 12:42:43 Mounting /Users/t/projects/python/playground/lambda-deep-dive/t-resume/t-resume/.aws-sam/build/ResumeFunction as /var/task:ro inside runtime container
START RequestId: 3a4f03ef-bfbb-1f74-b3e3-865485952465 Version: $LATEST
{'d': 'Please solve this puzzle:\n ABCD\nA=<--\nB>---\nC->--\nD<--=\n',
 'q': 'Puzzle'}
END RequestId: 3a4f03ef-bfbb-1f74-b3e3-865485952465
REPORT RequestId: 3a4f03ef-bfbb-1f74-b3e3-865485952465  Duration: 6.59 ms       Billed Duration: 100 ms Memory Size: 128 MB     Max Memory Used: 22 MB
2019-09-22 12:42:44 127.0.0.1 - - [22/Sep/2019 12:42:44] "GET /resume?q=Puzzle&d=Please+solve+this+puzzle%3A%0A+ABCD%0AA%3D%3C--%0AB%3E---%0AC-%3E--%0AD%3C--%3D%0A HTTP/1.0" 200 -
